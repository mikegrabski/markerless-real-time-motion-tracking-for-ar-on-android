package com.grabski.mike.featuretracking.utils

import com.grabski.mike.featuretracking.detector.Keypoint

/**
 * Created by mike on 5/20/18.
 *
 * Math helper methods.
 */
object MathUtil {

    fun getDistance(p1: Keypoint, p2: Keypoint)
            = Math.sqrt(((p1.positionX - p2.positionX) * (p1.positionX - p2.positionX) + (p1.positionY - p2.positionY) * (p1.positionY - p2.positionY)).toDouble())

}