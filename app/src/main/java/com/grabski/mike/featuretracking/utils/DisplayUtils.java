package com.grabski.mike.featuretracking.utils;

import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by mike on 2/25/18.
 *
 * Helper class for display specifics methods
 */

public class DisplayUtils {
    public static final float dpToPx(int dp, Resources resources) {
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, resources.getDisplayMetrics());
    }
}
