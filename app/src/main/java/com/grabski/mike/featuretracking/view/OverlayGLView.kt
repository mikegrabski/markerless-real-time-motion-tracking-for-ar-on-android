package com.grabski.mike.featuretracking.view

import android.content.Context
import android.graphics.BitmapFactory
import android.graphics.PixelFormat
import android.graphics.PointF
import android.opengl.GLSurfaceView
import android.opengl.GLU
import android.opengl.GLUtils
import android.opengl.Matrix.multiplyMM
import android.opengl.Matrix.transposeM
import android.util.AttributeSet
import android.util.Pair
import android.view.MotionEvent
import com.grabski.mike.featuretracking.R
import com.grabski.mike.featuretracking.detector.FeatureDetectorListener
import com.grabski.mike.featuretracking.detector.Keypoint
import com.grabski.mike.featuretracking.detector.ObjectTracker
import com.grabski.mike.featuretracking.utils.GRVCoordinates
import com.grabski.mike.featuretracking.utils.MathUtil
import java.nio.ByteBuffer
import java.nio.ByteOrder
import java.nio.FloatBuffer
import javax.microedition.khronos.egl.EGLConfig
import javax.microedition.khronos.opengles.GL10

/**
 * Created by mike.
 *
 * A class that draws the overlay image and uses rotation matrix to rotate it as well as
 * translation and scale data provided from the detector to translate and resize the image.
 *
 * Reason why we use GLES for this is that the rotation matrix provided by the sensor
 * fits flawlessly with the GL ModelView Matrix.
 *
 */
class OverlayGLView : GLSurfaceView {

    private lateinit var objectTracker: ObjectTracker
    private lateinit var renderer: GRVRenderer
    var shouldScale = false

    constructor(context: Context) : super(context) {
        init(context)
    }

    constructor(context: Context, attributeSet: AttributeSet) : super(context, attributeSet) {
        init(context)
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(measuredWidth, (measuredWidth * 1.33f).toInt())


    }

    private fun init(context: Context) {
        setEGLContextClientVersion(1)
        holder.setFormat(PixelFormat.TRANSLUCENT)
        setEGLConfigChooser(8, 8, 8, 8, 0, 0)
        setZOrderOnTop(true)// This is the important line
        renderer = GRVRenderer(context)
        setRenderer(renderer)
    }

    fun setDetector(objectTracker: ObjectTracker) {
        this.objectTracker = objectTracker
        this.objectTracker.addListener(object : FeatureDetectorListener<Keypoint> {
            override fun onKeypointsDetected(keyPoints: MutableList<Keypoint>?) {
            }

            override fun onKeypointsMatched(matchCount: Int, matchedKeypoints: MutableList<Pair<Keypoint, Keypoint>>) {
                if (matchCount == 0) return
                val dxList = matchedKeypoints.map { it.second.positionX - it.first.positionX }.sorted()
                val dyList = matchedKeypoints.map { it.second.positionY - it.first.positionY }.sorted()
                //Getting the median instead of the average
                val dx = dxList[dxList.size / 2] * 2f
                val dy = dyList[dyList.size / 2] * 2f
                renderer.translateBy(dx, dy)

                val dist = mutableListOf<Pair<Float, Float>>()
                for (i in 0 until matchCount - 1) {
                    dist.add(Pair(MathUtil.getDistance(matchedKeypoints[i].first, matchedKeypoints[i + 1].first).toFloat(), MathUtil.getDistance(matchedKeypoints[i].second, matchedKeypoints[i + 1].second).toFloat()))
                }
                val scales = dist.map { it.second / it.first }.sorted()
                if (scales.isNotEmpty()) renderer.applyScale(scales[scales.size / 2])
                requestRender()
            }
        })
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            renderer.resetSquare()
            return true
        }
        if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_MOVE) {
            renderer.setStartPosition(2f * event.x / width.toFloat() - 1f, 2f * event.y / height.toFloat() - 1)
            renderer.resetRotMCache()
            renderer.showimage()
        }
        return super.onTouchEvent(event)
    }
}

internal class GRVRenderer(context: Context) : GLSurfaceView.Renderer {
    private var square: Square? = null        // the square
    private val context: Context = context.applicationContext
    private val grvCoordinates: GRVCoordinates = GRVCoordinates(context)
    private var ratio: Float = 0.toFloat()
    private var show = false
    private var rotMCache: FloatArray? = null
    private var imageCoords = floatArrayOf(
            1f, -1f,  // V3 - bottom right
            -1f, -1f, // V1 - bottom left
            -1f, 1f, // V2 - top left
            1f, 1f // V4 - top right
    )
    private var newCoords = false
    var width: Int = 0
    var height: Int = 0


    fun resetRotMCache() {
        rotMCache = grvCoordinates.rotationMatrix
    }

    override fun onDrawFrame(gl: GL10) {
        // clear Screen and Depth Buffer
        gl.glClear(GL10.GL_COLOR_BUFFER_BIT or GL10.GL_DEPTH_BUFFER_BIT)

        // Reset the Modelview Matrix
        gl.glLoadIdentity()
        // Drawing
        gl.glTranslatef(0.0f, 0.0f, -5.0f)        // move 5 units INTO the screen
        // is the same as moving the camera 5 units away
        if (!show) {
            rotMCache = grvCoordinates.rotationMatrix
        } else {
            if (newCoords) {
                val coords = floatArrayOf(
                        //      x           y           z
                        imageCoords[4], -imageCoords[5], 0f,
                        imageCoords[6], -imageCoords[7], 0f,
                        imageCoords[0], -imageCoords[1], 0f,
                        imageCoords[2], -imageCoords[3], 0f)

                if (square == null) {
                    square = Square(coords)
                    square?.loadGLTexture(gl, context)
                }
            }
            square?.draw(gl)                        // Draw the triangle
        }

    }

    override fun onSurfaceChanged(gl: GL10, width: Int, height: Int) {
        var h = height
        if (height == 0) {                        //Prevent A Divide By Zero By
            h = 1                        //Making Height Equal One
        }

        gl.glViewport(0, 0, width, h)    //Reset The Current Viewport
        gl.glMatrixMode(GL10.GL_PROJECTION)    //Select The Projection Matrix
        gl.glLoadIdentity()                    //Reset The Projection Matrix
        this.width = width
        this.height = h
        ratio = width.toFloat() / h.toFloat()
        //in this case fov is 22.6f, and thee coords are
        //with the fact that we move the scene 5 points behind,
        //we get the visible screen to be
        // from -1 to 1 for y and -ratio to ratio for x
        GLU.gluPerspective(gl, 22.6f, ratio, 0.1f, 100.0f)
        gl.glMatrixMode(GL10.GL_MODELVIEW)    //Select The Modelview Matrix
        gl.glLoadIdentity()                    //Reset The Modelview Matrix
    }

    override fun onSurfaceCreated(gl: GL10, config: EGLConfig) {
        gl.glEnable(GL10.GL_TEXTURE_2D)            //Enable Texture Mapping ( NEW )
        gl.glShadeModel(GL10.GL_SMOOTH)            //Enable Smooth Shading
        gl.glClearColor(0.0f, 0.0f, 0.0f, 0.0f)    //Green Background
        gl.glClearDepthf(1.0f)                     //Depth Buffer Setup
        gl.glEnable(GL10.GL_DEPTH_TEST)            //Enables Depth Testing
        gl.glDepthFunc(GL10.GL_LEQUAL)             //The Type Of Depth Testing To Do
        gl.glHint(GL10.GL_PERSPECTIVE_CORRECTION_HINT, GL10.GL_NICEST)
    }

    fun showimage() {
        show = true
    }


    fun setStartPosition(x: Float, y: Float) {
        newCoords = true
        imageCoords = floatArrayOf(
                -0.2f + x, 0.2f + y,
                0.2f + x, 0.2f + y,
                -0.2f + x, -0.2f + y,
                0.2f + x, -0.2f + y)
    }

    private var translation: PointF = PointF(0f, 0f)

    fun translateBy(x: Float, y: Float) {
        square?.translateBy(x, y)
    }

    fun clearEverything() {
        show = false
    }

    internal inner class Square {

        var rotM = FloatArray(16)
        var currVertex = FloatArray(12)
        private var vertexBuffer: FloatBuffer? = null    // buffer holding the vertices
        private val textures = IntArray(1)
        private var textureBuffer: FloatBuffer? = null    // buffer holding the texture coordinates
        private val texture = floatArrayOf(
                // Mapping coordinates for the vertices
                1.0f, 0.0f, // bottom left	(V1)
                0.0f, 0.0f, // top left		(V2)
                // bottom right	(V3)
                1.0f, 1.0f,
                0.0f, 1.0f// top right	(V4)
        )
        private var prevVertex = FloatArray(12)
        private var scale: Float = 1f

        private var isTextureLoaded: Boolean = false

        //TODO To replace fixed values with ratio
        private var vertices = floatArrayOf(-ratio, -1f, 0.0f, // V1 - bottom left
                -ratio, 1f, 0.0f, // V2 - top left
                ratio, -1f, 0.0f, // V3 - bottom right
                ratio, 1f, 0.0f            // V4 - top right
        )

        val currentPosition: FloatArray
            get() {
                val positions = FloatArray(12)
                vertexBuffer!!.get(positions)
                vertexBuffer!!.position(0)
                return floatArrayOf(positions[9] / ratio, -positions[10], positions[3] / ratio, -positions[4], positions[6] / ratio, -positions[7], positions[0] / ratio, -positions[1])
            }


        constructor(vertices: FloatArray) {
            var byteBuffer = ByteBuffer.allocateDirect(vertices.size * 4)
            byteBuffer.order(ByteOrder.nativeOrder())
            vertexBuffer = byteBuffer.asFloatBuffer()
            vertexBuffer!!.put(vertices)
            vertexBuffer!!.position(0)

            byteBuffer = ByteBuffer.allocateDirect(texture.size * 4)
            byteBuffer.order(ByteOrder.nativeOrder())
            textureBuffer = byteBuffer.asFloatBuffer()
            textureBuffer!!.put(texture)
            textureBuffer!!.position(0)


        }

        fun loadGLTexture(gl: GL10, context: Context) {
            if (isTextureLoaded) {
                return
            }
            // loading texture
            val bitmap = BitmapFactory.decodeResource(context.resources,
                    R.drawable.texture)

            // generate one texture pointer
            gl.glGenTextures(1, textures, 0)
            // ...and bind it to our array
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0])

            // create nearest filtered texture
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST.toFloat())
            gl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_LINEAR.toFloat())

            // Use Android GLUtils to specify a two-dimensional texture image from our bitmap
            GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0)

            // Clean up
            bitmap.recycle()
            isTextureLoaded = true
        }

        /**
         * The draw method for the square with the GL context
         */
        fun draw(gl: GL10) {

            rotM = grvCoordinates.rotationMatrix
            gl.glColor4f(0.8f, 1f, 1f, 1f)
            // bind the previously generated texture
            gl.glBindTexture(GL10.GL_TEXTURE_2D, textures[0])

            // Point to our buffers
            gl.glEnableClientState(GL10.GL_VERTEX_ARRAY)
            gl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY)

            // Set the face rotation
            gl.glFrontFace(GL10.GL_CW)

            // Point to our vertex buffer
            gl.glVertexPointer(3, GL10.GL_FLOAT, 0, vertexBuffer)
            gl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, textureBuffer)

            val rotMCacheT = FloatArray(16)
            val result = FloatArray(16)
            transposeM(rotMCacheT, 0, rotMCache!!.clone(), 0)
            multiplyMM(result, 0, rotM.clone(), 0, rotMCacheT, 0)
            gl.glMatrixMode(GL10.GL_MODELVIEW)
            gl.glMultMatrixf(result, 0)
            gl.glTranslatef(-0.2f * result[8] + translation.x, -0.5f * result[9] / ratio + translation.y, 0f)
            gl.glScalef(scale, scale, 1f)

            // Draw the vertices as triangle strip
            gl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, vertices.size / 3)

            //Disable the client state before leaving
            gl.glDisableClientState(GL10.GL_VERTEX_ARRAY)
            gl.glDisableClientState(GL10.GL_TEXTURE_COORD_ARRAY)
        }

        fun translateBy(x: Float, y: Float) {
            translation = PointF(translation.x + x, translation.y - y)
        }

        fun resetTranslation() {
            translation = PointF(0f, 0f)
        }

        fun resetScale() {
            scale = 1f
        }

        fun applyScale(fl: Float) {
            scale *= fl
        }
    }

    fun resetSquare() {
        square?.resetTranslation()
        square?.resetScale()
    }

    fun applyScale(fl: Float) {
        square?.applyScale(fl)
    }
}

