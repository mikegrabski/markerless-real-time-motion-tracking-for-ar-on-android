package com.grabski.mike.featuretracking.utils;

/**
 * Created by mike on 2/18/18.
 * ============================================================
 * PLEASE place all constant values here, things like matching threshold and stuff.
 */

public class C {

    public static final int PERMISSION_CAMERA = 10001;

    public static final int CAPTURE_MAX_SIZE = 300;

    public static final int OPENCV_FRAME_SIZE = 320;

    public static int ORB_MATCH_COUNT = 10;
}
