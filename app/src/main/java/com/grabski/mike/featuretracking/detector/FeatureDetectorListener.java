package com.grabski.mike.featuretracking.detector;

import android.util.Pair;

import java.util.List;

/**
 * Created by mike on 2/18/18.
 *
 * Listener that helps with detecting all the keypoints that were detected and matched.
 *
 */

public interface FeatureDetectorListener<T extends Keypoint> {
    public void onKeypointsDetected(List<T> keyPoints);

    public void onKeypointsMatched(int matchCount, List<Pair<T, T>> matchedKeypoints);
}
