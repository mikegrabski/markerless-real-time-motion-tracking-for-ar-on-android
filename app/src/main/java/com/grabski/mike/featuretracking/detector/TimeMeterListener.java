package com.grabski.mike.featuretracking.detector;

/**
 * Created by mike on 2/25/18.
 *
 * Listener to help with detecting time taken for each frame processing
 */

public interface TimeMeterListener {
    /**
     * @param elapsedTime elapsed time in ms
     */
    void onFrameProcessed(long elapsedTime);
}
