package com.grabski.mike.featuretracking.view;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.opengl.GLES11Ext;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.opengl.Matrix;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;

import com.grabski.mike.featuretracking.R;
import com.grabski.mike.featuretracking.utils.camera.CameraUtils;
import com.grabski.mike.featuretracking.utils.camera.FrameContainer;
import com.grabski.mike.featuretracking.utils.camera.Shader;

import java.io.IOException;
import java.nio.ByteBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

/**
 * Created by mike on 2/18/18.
 * =============================
 * <p>
 * Currently not used, instead CameraSurfaceView is used.
 *
 * */

@Deprecated
public class CameraGLRenderer extends GLSurfaceView implements
        GLSurfaceView.Renderer,
        SurfaceTexture.OnFrameAvailableListener {

    private final Shader cameraShader = new Shader();
    private final Shader bitmapShader = new Shader();
    private Context context;
    /**
     * Camera and SurfaceTexture
     */
    private Camera camera;
    private SurfaceTexture surfaceTexture;
    private int surfaceWidth, surfaceHeight;
    private boolean updateTexture = false;

    /**
     * OpenGL params
     */

    private ByteBuffer imageQuadVertices;
    private float[] imageTransformM = new float[16];
    private float[] imageOrientationM = new float[16];
    private float[] imageRatio = new float[2];

    private ByteBuffer cameraQuadVertices;
    private float[] cameraTransformM = new float[16];
    private float[] cameraOrientationM = new float[16];
    private float[] cameraRatio = new float[2];

    /**
     * handle
     */
    private int cameraTextureHandle;
    private int imageTextureHandle;

    //Latest pixels from image
    private FrameContainer frameContainer;

    private Bitmap bitmap;

    public CameraGLRenderer(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public CameraGLRenderer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        //Create full scene quad buffer
        final byte FULL_QUAD_COORDS[] = {-1, 1, -1, -1, 1, 1, 1, -1};
        frameContainer = new FrameContainer();

        cameraQuadVertices = ByteBuffer.allocateDirect(4 * 2);
        cameraQuadVertices.put(FULL_QUAD_COORDS).position(0);

        imageQuadVertices = ByteBuffer.allocateDirect(4 * 2);
        imageQuadVertices.put(FULL_QUAD_COORDS).position(0);

//        bitmap = BitmapFactory.decodeResource(context.getResources(), R.drawable.goro);
        setPreserveEGLContextOnPause(true);
        setEGLContextClientVersion(2);
        setRenderer(this);
        setRenderMode(GLSurfaceView.RENDERMODE_WHEN_DIRTY);
    }

    @Override
    public synchronized void onFrameAvailable(SurfaceTexture surfaceTexture) {
        updateTexture = true;
        requestRender();
    }


    @Override
    public synchronized void onSurfaceCreated(GL10 gl, EGLConfig config) {
        //load and compile cameraShader

        try {
            cameraShader.setProgram(R.raw.vshader, R.raw.fshader, context);
            bitmapShader.setProgram(R.raw.vshader, R.raw.fshaderimage, context);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

    @SuppressLint("NewApi")
    @Override
    public synchronized void onSurfaceChanged(GL10 gl, int width, int height) {
        this.surfaceWidth = width;
        this.surfaceHeight = height;

        //generate camera texture------------------------
        int[] mTextureHandles = new int[2];
        GLES20.glGenTextures(2, mTextureHandles, 0);
        cameraTextureHandle = mTextureHandles[0];

        GLES20.glBindTexture(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, mTextureHandles[0]);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_S, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_WRAP_T, GLES20.GL_CLAMP_TO_EDGE);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES11Ext.GL_TEXTURE_EXTERNAL_OES, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);

        imageTextureHandle = mTextureHandles[1];

        GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextureHandle);
//        // Set filtering
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MIN_FILTER, GLES20.GL_LINEAR);
        GLES20.glTexParameteri(GLES20.GL_TEXTURE_2D, GLES20.GL_TEXTURE_MAG_FILTER, GLES20.GL_LINEAR);
//
//        // Load the bitmap into the bound texture.
        GLUtils.texImage2D(GLES20.GL_TEXTURE_2D, 0, bitmap, 0);


        //set up surfacetexture------------------
        SurfaceTexture oldSurfaceTexture = surfaceTexture;
        surfaceTexture = new SurfaceTexture(cameraTextureHandle);
        surfaceTexture.setOnFrameAvailableListener(this);
        if (oldSurfaceTexture != null) {
            oldSurfaceTexture.release();
        }


        //set camera para-----------------------------------
        int camera_width = 0;
        int camera_height = 0;

        if (camera != null) {
            camera.stopPreview();
            camera.release();
            camera = null;
        }

        camera = Camera.open();

        camera.setDisplayOrientation(90);

        try {
            camera.setPreviewTexture(surfaceTexture);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

        Camera.Parameters param = camera.getParameters();
        final Camera.Size pSize = CameraUtils.getBestSize(param.getSupportedPreviewSizes());
        param.setPreviewSize(pSize.width, pSize.height);
        camera.setPreviewCallback(new Camera.PreviewCallback() {
            @Override
            public void onPreviewFrame(byte[] data, Camera camera) {
                Log.e("Frame Recieve time", ":");
                long start = System.currentTimeMillis();
                frameContainer.putPixels(data);
                Log.i(this.getClass().getSimpleName(), "Time taken to process frame: " + (System.currentTimeMillis() - start));
            }
        });
        param.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO);
        //get the camera orientation and display dimension------------
        if (context.getResources().getConfiguration().orientation ==
                Configuration.ORIENTATION_PORTRAIT) {
            Matrix.setRotateM(cameraOrientationM, 0, 0f, 0f, 0f, 1f);
            cameraRatio[1] = 1;
            cameraRatio[0] = 1;
        } else {
            Matrix.setRotateM(cameraOrientationM, 0, 90f, 0f, 0f, 1f);
            cameraRatio[1] = 1;
            cameraRatio[0] = 1;
        }

        //start camera-----------------------------------------
        camera.setParameters(param);
        camera.startPreview();

        frameContainer.setup(pSize.height, pSize.width);
        //start render---------------------
        requestRender();
    }

    @Override
    public synchronized void onDrawFrame(GL10 gl) {
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 0.0f);
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT);

        //render the texture to FBO if new frame is available
        if (updateTexture) {
            surfaceTexture.updateTexImage();
            surfaceTexture.getTransformMatrix(cameraTransformM);

            updateTexture = false;

            GLES20.glViewport(0, 0, surfaceWidth, surfaceHeight);

            cameraShader.useProgram();
            int uTransformM = cameraShader.getHandle("uTransformM");
            int uOrientationM = cameraShader.getHandle("uOrientationM");
            int uRatioV = cameraShader.getHandle("ratios");

            GLES20.glUniformMatrix4fv(uTransformM, 1, false, cameraTransformM, 0);
            GLES20.glUniformMatrix4fv(uOrientationM, 1, false, cameraOrientationM, 0);
            GLES20.glUniform2fv(uRatioV, 1, cameraRatio, 0);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, cameraTextureHandle);

            renderQuad(cameraShader.getHandle("aPosition"), cameraQuadVertices);

            //////////////////////||||||||||||||||||||\\\\\\\\\\\\\\\\\\\\\\\\\\

            bitmapShader.useProgram();
            uTransformM = bitmapShader.getHandle("uTransformM");
            uOrientationM = bitmapShader.getHandle("uOrientationM");
            uRatioV = bitmapShader.getHandle("ratios");

            GLES20.glUniformMatrix4fv(uTransformM, 1, false, imageTransformM, 0);
            GLES20.glUniformMatrix4fv(uOrientationM, 1, false, imageOrientationM, 0);
            GLES20.glUniform2fv(uRatioV, 1, imageRatio, 0);

            GLES20.glActiveTexture(GLES20.GL_TEXTURE0);
            GLES20.glBindTexture(GLES20.GL_TEXTURE_2D, imageTextureHandle);

            renderQuad(bitmapShader.getHandle("aPosition"), imageQuadVertices);
            //Too slow, 90ms per frame on s7 Edge
//            GLES20.glReadPixels(0, 0, surfaceWidth, surfaceHeight, GLES20.GL_RGBA, GLES20.GL_UNSIGNED_BYTE, frameContainer.getBuffer());
        }

    }

    private void renderQuad(int aPosition, ByteBuffer vertexBuffer) {
        GLES20.glVertexAttribPointer(aPosition, 2, GLES20.GL_BYTE, false, 0, vertexBuffer);
        GLES20.glEnableVertexAttribArray(aPosition);
        GLES20.glDrawArrays(GLES20.GL_TRIANGLE_STRIP, 0, 4);
    }

    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void onDestroy() {
        updateTexture = false;
        surfaceTexture.release();
        if (camera != null) {
            camera.stopPreview();
            camera.setPreviewCallback(null);
            camera.release();
        }

        camera = null;
    }

    public com.grabski.mike.featuretracking.utils.camera.FrameContainer getFrameContainer() {
        return frameContainer;
    }

}
