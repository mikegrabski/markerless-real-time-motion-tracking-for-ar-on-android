package com.grabski.mike.featuretracking.detector;

import android.util.Log;
import android.util.Pair;

import com.grabski.mike.featuretracking.utils.C;
import com.grabski.mike.featuretracking.utils.camera.FrameContainer;

import org.opencv.core.DMatch;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.MatOfKeyPoint;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.features2d.ORB;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by mike on 2/20/18.
 *
 * This implements ORB tracker using OpenCV's helper functions.
 *
 */

public class ORBOpenCVTracker extends ObjectTracker {
    private static final String TAG = ObjectTracker.class.getSimpleName();

    private ORB orb;

    private MatOfKeyPoint currentKeypoints;
    private MatOfKeyPoint previousKeypoints;

    private Mat currentDescriptors;
    private Mat previousDescriptors;

    private Mat currentImage;

    private DescriptorMatcher matcher;
    private MatOfDMatch matches;

    public ORBOpenCVTracker(FrameContainer frameContainer) {
        super(frameContainer);
        orb = ORB.create(250, 1.2f, 8, 31, 0, 2, ORB.HARRIS_SCORE, 31, 20);
        matcher = DescriptorMatcher.create(DescriptorMatcher.BRUTEFORCE_HAMMING);
        currentKeypoints = new MatOfKeyPoint();
        previousKeypoints = new MatOfKeyPoint();
        currentDescriptors = new Mat();
        previousDescriptors = new Mat();
        matches = new MatOfDMatch();

//        int nfeatures = 500, float scaleFactor = 1.2f, int nlevels = 8, int edgeThreshold = 31, int firstLevel = 0, int WTA_K = 2, int scoreType = ORB::HARRIS_SCORE, int patchSize = 31, int fastThreshold = 20
//        script = RenderScript.create(App.Companion.getContext());
    }

    @Override
    public void startTracking() {
        isTracking = true;
        Thread th = new Thread(new Runnable() {
            @Override
            public void run() {
                while (isTracking) {
                    currentImage = frameContainer.getInputMat();
                    long startTime = System.currentTimeMillis();
                    orb.detect(currentImage, currentKeypoints);
                    orb.compute(currentImage, currentKeypoints, currentDescriptors);
                    if (!previousDescriptors.empty() && !previousKeypoints.empty()) {
                        doMatching();
                    }
                    previousKeypoints = new MatOfKeyPoint(currentKeypoints.clone());
                    previousDescriptors = currentDescriptors.clone();
                    if (timeMeterListener != null) {
                        timeMeterListener.onFrameProcessed(System.currentTimeMillis() - startTime);
                        Log.i(this.getClass().getSimpleName(), "Time taken to detect frame: " + (System.currentTimeMillis() - startTime));
                    }
                }
            }
        });
        th.setPriority(Thread.MAX_PRIORITY); //No difference observed with this line, but since it didn't break anything keeping it.
        th.start();
    }

    /**
     * Performs matching with detected descriptors
     */
    private void doMatching() {
        matcher.match(currentDescriptors, previousDescriptors, matches);
        List<DMatch> matchesList = matches.toList();
        List<Pair<Keypoint, Keypoint>> keypointMatches = new ArrayList<>();

        if (matchesList.size() == 0) {
            onKeypointsMatched(matchesList.size(), keypointMatches);
            return;
        }


        Collections.sort(matchesList, new Comparator<DMatch>() {
            @Override
            public int compare(DMatch m1, DMatch m2) {
                return Float.compare(m1.distance, m2.distance);
            }
        });

        List<DMatch> good_matches = matchesList.subList(0, C.ORB_MATCH_COUNT < matchesList.size() ? C.ORB_MATCH_COUNT : (matchesList.size() - 1));
        for (DMatch dMatch : good_matches) {
            keypointMatches.add(new Pair<>(Keypoint.fromPoint(previousKeypoints.toArray()[dMatch.trainIdx].pt, (int) (C.OPENCV_FRAME_SIZE * frameContainer.ratio), C.OPENCV_FRAME_SIZE),
                    Keypoint.fromPoint(currentKeypoints.toArray()[dMatch.queryIdx].pt, (int) (C.OPENCV_FRAME_SIZE * frameContainer.ratio), C.OPENCV_FRAME_SIZE)));
        }
        onKeypointsMatched(good_matches.size(), keypointMatches);
    }

    @Override
    public void stopTracking() {
        isTracking = false;
    }
}
