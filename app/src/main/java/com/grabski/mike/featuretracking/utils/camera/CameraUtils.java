package com.grabski.mike.featuretracking.utils.camera;

import android.content.Context;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.os.Build;
import android.view.Surface;
import android.view.WindowManager;

import com.grabski.mike.featuretracking.utils.C;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.TreeMap;

import static android.hardware.Camera.CameraInfo.CAMERA_FACING_FRONT;

/**
 * Created by MikeG.
 *
 * Helper class for camera helper methods.
 */

public class CameraUtils {

    public static Camera.Size getBestSize(List<Camera.Size> availableSizes) {
        List<Camera.Size> ratio3x4s = new ArrayList<>();
        for (Camera.Size size : availableSizes) {
            // Ensure the width is always smaller than height because it's messed up on some devices
            if (size.width * 3 == size.height * 4) {
                ratio3x4s.add(size);
            }
        }

        Collections.sort(ratio3x4s, new Comparator<Camera.Size>() {
            @Override
            public int compare(Camera.Size o1, Camera.Size o2) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                    return Integer.compare(o1.width, o2.width);
                } else {
                    return Float.compare(o1.width, o2.width);
                }
            }
        });

        Camera.Size outSize = null;
        for (Camera.Size size : ratio3x4s) {
            outSize = size;
            if (size.height > C.CAPTURE_MAX_SIZE) {
                break;
            }
        }

        return outSize;
    }

    public static int getCameraDisplayOrientation(Context context, int cameraId) {
        Camera.CameraInfo info = new Camera.CameraInfo();
        Camera.getCameraInfo(cameraId, info);

        int rotation = ((WindowManager) context.getSystemService(Context.WINDOW_SERVICE)).getDefaultDisplay().getOrientation();
        int degrees = 0;
        switch (rotation) {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result;
        if (info.facing == CAMERA_FACING_FRONT) {
            result = (info.orientation + degrees) % 360;
            result = (360 - result) % 360;  // compensate the mirror
        } else {  // back-facing
            result = (info.orientation - degrees + 360) % 360;
        }

        return result;
    }

    public static Camera.Size getBestAspectPreviewSize(int displayOrientation, int width, int height, Camera.Parameters parameters) {

        double closeEnough = 0.1;
        double targetRatio = (double) width / height;
        Camera.Size bestSize = null;

        if (displayOrientation == 90 || displayOrientation == 270) {
            targetRatio = (double) height / width;
        }

        List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();
        TreeMap<Double, List<Camera.Size>> diffs = new TreeMap<>();


        for (Camera.Size size : sizes) {

            double ratio = (double) size.width / size.height;

            double diff = Math.abs(ratio - targetRatio);
            if (diff < closeEnough) {
                if (diffs.keySet().contains(diff)) {
                    //add the value to the list
                    diffs.get(diff).add(size);
                } else {
                    List<Camera.Size> newList = new ArrayList<>();
                    newList.add(size);
                    diffs.put(diff, newList);
                }
            }
        }

        //diffs now contains all of the usable sizes
        //now let's see which one has the least amount of
        for (List<Camera.Size> entries : diffs.values()) {
            for (Camera.Size size : entries) {
                if (size.width >= width && size.height >= width) {
                    bestSize = size;
                }
            }
        }

        //if we don't have bestSize then just use whatever the default was to begin with
        if (bestSize == null) {
            if (parameters.getPreviewSize() != null) {
                bestSize = parameters.getPreviewSize();
                return bestSize;
            }

            //pick the smallest difference in ratio?  or pick the largest resolution?
            //right now we are just picking the lowest ratio difference
            for (List<Camera.Size> size : diffs.values()) {
                for (Camera.Size s : size) {
                    if (bestSize == null) {
                        bestSize = s;
                        break;
                    }
                }
            }
        }

        return bestSize;
    }

    public static String getFocusBestMode(List<String> focusModes) {
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE)) {
            return Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE;
        }
        if (focusModes.contains(Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO)) {
            return Camera.Parameters.FOCUS_MODE_CONTINUOUS_VIDEO;
        }
        return Camera.Parameters.FOCUS_MODE_FIXED;
    }

    public static boolean isFrontCameraAvailable() {
        int numCameras = Camera.getNumberOfCameras();
        for (int i = 0; i < numCameras; i++) {
            Camera.CameraInfo info = new Camera.CameraInfo();
            Camera.getCameraInfo(i, info);
            if (CAMERA_FACING_FRONT == info.facing) {
                return true;
            }
        }
        return false;
    }

    public static boolean isFlashAvailable(Camera.Parameters parameters) {
        List<String> flashModes = parameters.getSupportedFlashModes();
        if (flashModes != null && flashModes.size() >= 2) {
            return true;
        }
        return false;
    }

    public static boolean hasFlash(Context context) {
        return context.getPackageManager().hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }
}
