package com.grabski.mike.featuretracking.detector;

import org.opencv.core.Point;

/**
 * Created by mike on 2/18/18.
 *
 */

public class Keypoint {
    private float positionX, positionY;

    public Keypoint(float x, float y) {
        positionX = x;
        positionY = y;
    }

    static Keypoint fromPoint(Point pt, int width, int height) {
        return new Keypoint((float) (pt.x / width), (float) (pt.y / height));
    }

    public float getPositionX() {
        return positionX;
    }

    public void setPositionX(float positionX) {
        this.positionX = positionX;
    }

    public float getPositionY() {
        return positionY;
    }

    public void setPositionY(float positionY) {
        this.positionY = positionY;
    }
}
