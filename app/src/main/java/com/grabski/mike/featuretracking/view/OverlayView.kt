package com.grabski.mike.featuretracking.view

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.util.Pair
import android.view.MotionEvent
import android.view.View
import com.grabski.mike.featuretracking.detector.FeatureDetectorListener
import com.grabski.mike.featuretracking.detector.Keypoint
import com.grabski.mike.featuretracking.detector.ObjectTracker
import com.grabski.mike.featuretracking.utils.DisplayUtils

/**
 * Created by MikeG on 2/23/18.
 */

class OverlayView : View {

    var objectTracker: ObjectTracker? = null
    var matchedKeypoints: MutableList<Pair<Keypoint, Keypoint>> = ArrayList()

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    private val keypointPaint: Paint = Paint()
    private val movementPaint: Paint = Paint()

    private val movementPath: Path = Path()
    private lateinit var currentPoint: PointF
    private lateinit var goroBitmap: Bitmap
    private var drawBitmap: Boolean = false

    fun initWithDetector(objectTracker: ObjectTracker) {
        this.objectTracker = objectTracker
        keypointPaint.color = Color.RED
        movementPaint.color = Color.BLUE
        keypointPaint.style = Paint.Style.FILL
        movementPaint.style = Paint.Style.STROKE
        currentPoint = PointF(width.toFloat() / 2, height.toFloat() / 2)
//        goroBitmap = BitmapFactory.decodeResource(resources, R.drawable.goro)
        movementPath.moveTo(currentPoint.x, currentPoint.y)
        movementPaint.strokeWidth = DisplayUtils.dpToPx(3, resources)
        objectTracker.addListener(object : FeatureDetectorListener<Keypoint> {
            override fun onKeypointsDetected(keyPoints: MutableList<Keypoint>?) {
            }

            override fun onKeypointsMatched(matchCount: Int, matchedKeypoints: MutableList<Pair<Keypoint, Keypoint>>?) {
                this@OverlayView.matchedKeypoints = ArrayList(matchedKeypoints)
                updatePath()
                this@OverlayView.postInvalidateOnAnimation()
            }
        })
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (objectTracker?.isTracking == true) {
            for (i in matchedKeypoints) {
                canvas.drawCircle(i.second.positionX * width, i.second.positionY * height, DisplayUtils.dpToPx(2, resources), keypointPaint)
            }
            if (drawBitmap) {
                canvas.drawBitmap(goroBitmap, currentPoint.x - goroBitmap.width / 2, currentPoint.y - goroBitmap.height / 2, keypointPaint)
            }
        }
    }

    override fun onMeasure(widthMeasureSpec: Int, heightMeasureSpec: Int) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec)
        setMeasuredDimension(measuredWidth, (measuredWidth * 1.33f).toInt())
    }

    override fun onTouchEvent(event: MotionEvent): Boolean {
        if (event.action == MotionEvent.ACTION_DOWN) {
            return true
        }
        if (event.action == MotionEvent.ACTION_UP || event.action == MotionEvent.ACTION_MOVE) {
            currentPoint = PointF(event.x, event.y)
            drawBitmap = true
        }
        return super.onTouchEvent(event)
    }

    fun updatePath() {
        var dx = 0f
        var dy = 0f
        for (item in matchedKeypoints) {
            dx += item.second.positionX - item.first.positionX
            dy += item.second.positionY - item.first.positionY
        }
        dx /= matchedKeypoints.size
        dy /= matchedKeypoints.size
        dx *= width
        dy *= height
        currentPoint = PointF(currentPoint.x + dx, currentPoint.y + dy)
        movementPath.lineTo(currentPoint.x, currentPoint.y)
    }

    fun reset() {
        movementPath.reset()
        matchedKeypoints.clear()
        drawBitmap = false
        currentPoint = PointF(width.toFloat() / 2, height.toFloat() / 2)
        movementPath.moveTo(currentPoint.x, currentPoint.y)
    }
}
