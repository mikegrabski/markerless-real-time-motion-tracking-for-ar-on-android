package com.grabski.mike.featuretracking.view;

import android.content.Context;
import android.hardware.Camera;
import android.os.Build;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

import com.grabski.mike.featuretracking.utils.camera.CameraUtils;
import com.grabski.mike.featuretracking.utils.camera.FrameContainer;

import java.util.List;

/**
 * Created by Mike.
 * <p>
 * A simpler Camera preview class with fixed preview ratio
 */
public class CameraSurfaceView extends SurfaceView implements SurfaceHolder.Callback, Camera.PreviewCallback {


    private Context context;
    private Camera camera;
    private SurfaceHolder holder;

    //Latest pixels from image
    private FrameContainer frameContainer;

    public CameraSurfaceView(Context context) {
        super(context);
        initialize(context);
    }


    public CameraSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        initialize(context);
    }

    public CameraSurfaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs);
        initialize(context);
    }

    public CameraSurfaceView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs);
        initialize(context);
    }

    private void initialize(Context context) {
        this.context = context;
        frameContainer = new FrameContainer();
        holder = getHolder();
        holder.addCallback(this);
    }


    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        try {
            camera.setPreviewDisplay(holder);
            camera.startPreview();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), (int) (getMeasuredWidth() * 1.33f));
    }

    @Override
    public void onPreviewFrame(byte[] data, Camera camera) {
        long start = System.currentTimeMillis();
        frameContainer.putPixels(data);
        Log.i(this.getClass().getSimpleName(), "Time taken to process frame: " + (System.currentTimeMillis() - start));
    }

    public void pause() {
        if (camera == null)
            return;
        camera.stopPreview();
        camera.setPreviewCallback(null);
        camera.release();
    }

    public void initCamera() {
        camera = Camera.open();


        Camera.Parameters params;
        params = camera.getParameters();

        List<Camera.Size> previewSizes = params.getSupportedPreviewSizes();
        Camera.Size pSize = CameraUtils.getBestSize(previewSizes);

        setMeasuredDimension(getMeasuredWidth(), getMeasuredHeight() * (pSize.height / pSize.width));
        requestLayout();

        params.setPreviewSize(pSize.width, pSize.height);
        params.setFocusMode(Camera.Parameters.FOCUS_MODE_CONTINUOUS_PICTURE);

        frameContainer.setup(pSize.height, pSize.width);

        camera.setParameters(params);
        camera.setPreviewCallback(this);

        if (Build.MODEL.equals("Nexus 5X")) {
            // rotate camera 180°
            camera.setDisplayOrientation(270);
        } else {


            camera.setDisplayOrientation(90);
        }
    }

    public com.grabski.mike.featuretracking.utils.camera.FrameContainer getFrameContainer() {
        return frameContainer;
    }

}
