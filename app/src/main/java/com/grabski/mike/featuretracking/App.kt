package com.grabski.mike.featuretracking

import android.annotation.SuppressLint
import android.app.Application
import android.content.Context
import android.util.Log
import org.opencv.android.OpenCVLoader

/**
 * Created by mike on 2/20/18.
 *
 *
 */
class App : Application() {

    /**
     * loads openCV lib
     */
    override fun onCreate() {
        if (!OpenCVLoader.initDebug()) Log.d("ERROR", "Unable to load OpenCV")
        else Log.d("SUCCESS", "OpenCV loaded")
        super.onCreate()
        context = this
    }

    companion object {
        @SuppressLint("StaticFieldLeak")
        var context: Context? = null;
    }
}