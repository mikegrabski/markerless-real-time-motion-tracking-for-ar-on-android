package com.grabski.mike.featuretracking.detector;

import android.util.Pair;

import com.grabski.mike.featuretracking.utils.camera.FrameContainer;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by mike on 2/18/18.
 * =================================
 * To be implemented by each individual detector.
 * <p>
 * As generic as possible so that later changing detectors would be really easy
 */

public abstract class ObjectTracker {
    protected FrameContainer frameContainer;
    protected List<FeatureDetectorListener> listeners;
    protected TimeMeterListener timeMeterListener;
    protected boolean isTracking = false;


    public ObjectTracker(FrameContainer frameContainer) {
        this.frameContainer = frameContainer;
        listeners = new ArrayList<>();
    }

    public void addListener(FeatureDetectorListener listener) {
        this.listeners.add(listener);
    }

    public abstract void startTracking();

    public abstract void stopTracking();

    public <T extends Keypoint> void onKeypointsDetected(List<T> keyPoints) {
        for (FeatureDetectorListener listener : listeners) {
            listener.onKeypointsDetected(keyPoints);
        }
    }

    public <T extends Keypoint> void onKeypointsMatched(int matchCount, List<Pair<T, T>> matchedKeypoints) {
        for (FeatureDetectorListener listener : listeners) {
            listener.onKeypointsMatched(matchCount, matchedKeypoints);
        }
    }

    public boolean isTracking() {
        return isTracking;
    }

    public void setTimeMeterListener(TimeMeterListener timeMeterListener) {
        this.timeMeterListener = timeMeterListener;
    }
}
