package com.grabski.mike.featuretracking.utils.camera;

import android.content.Context;
import android.renderscript.Allocation;
import android.renderscript.Element;
import android.renderscript.RenderScript;
import android.renderscript.ScriptIntrinsicYuvToRGB;
import android.renderscript.Type;

import com.grabski.mike.featuretracking.App;
import com.grabski.mike.featuretracking.utils.C;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgproc.Imgproc;

import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

/**
 * Created by mike on 2/20/18.
 * =================================
 * Contains latest available frame from the camera.
 * Call getPixels() to get most recent pixels.
 */

public class FrameContainer {
    //Since object detection is performed in another thread we need to have the frame ready
    private static final ReadWriteLock syncLock = new ReentrantReadWriteLock();
    public float ratio = 1;
    private int height, width;
    private byte[] rawData;
    private byte[] rgbData;
    private Mat inputMat;
    private Mat grayMat;

    /**
     * package private
     */
    public FrameContainer(int height, int width) {
        setup(height, width);
    }

    public FrameContainer() {
        //nothing here
    }

    public void setup(int height, int width) {
        this.height = height;
        this.width = width;
        inputMat = new Mat(height + height / 2, width, CvType.CV_8UC1);
        grayMat = new Mat();
        rgbData = new byte[height * width];
        ratio = (float) height / (float) width;
    }

    public void putPixels(final byte data[]) {
        rawData = data;
        inputMat.put(0, 0, data);
        if (inputMat.height() > 0 && inputMat.width() > 0) {
            syncLock.writeLock().lock();
            Imgproc.cvtColor(inputMat, grayMat, Imgproc.COLOR_YUV2GRAY_NV21);
            Core.transpose(grayMat, grayMat);
            Core.flip(grayMat, grayMat, 1);
            Imgproc.resize(grayMat, grayMat, new Size(C.OPENCV_FRAME_SIZE * ratio, C.OPENCV_FRAME_SIZE));
            Imgproc.blur(grayMat, grayMat, new Size(5, 5));
            syncLock.writeLock().unlock();
        }
    }

    public byte[] getPixels() {
        return rawData;
    }

    public byte[] getRgbData() {
        Allocation bmData = renderScriptNV21ToRGBA888(
                App.Companion.getContext(),
                width,
                height,
                rawData);
        bmData.copyTo(rgbData);
        return rgbData;
    }

    public Mat getInputMat() {
        syncLock.readLock().lock();
        Mat rgbCopy = grayMat.clone();
        syncLock.readLock().unlock();
        return rgbCopy;
    }

    private Allocation renderScriptNV21ToRGBA888(Context context, int width, int height, byte[] nv21) {
        RenderScript rs = RenderScript.create(context);
        ScriptIntrinsicYuvToRGB yuvToRgbIntrinsic = ScriptIntrinsicYuvToRGB.create(rs, Element.U8_4(rs));

        Type.Builder yuvType = new Type.Builder(rs, Element.U8(rs)).setX(nv21.length);
        Allocation in = Allocation.createTyped(rs, yuvType.create(), Allocation.USAGE_SCRIPT);

        Type.Builder rgbaType = new Type.Builder(rs, Element.RGBA_8888(rs)).setX(width).setY(height);
        Allocation out = Allocation.createTyped(rs, rgbaType.create(), Allocation.USAGE_SCRIPT);

        in.copyFrom(nv21);

        yuvToRgbIntrinsic.setInput(in);
        yuvToRgbIntrinsic.forEach(out);
        return out;
    }

    public Size getSize() {
        return new Size(width, height);
    }
}
