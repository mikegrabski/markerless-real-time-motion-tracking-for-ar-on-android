package com.grabski.mike.featuretracking

import android.content.pm.PackageManager
import android.os.Bundle
import android.support.v4.app.ActivityCompat
import android.support.v4.content.ContextCompat
import android.support.v7.app.AppCompatActivity
import android.util.Pair
import android.view.View
import android.widget.Button
import android.widget.TextView
import com.grabski.mike.featuretracking.detector.FeatureDetectorListener
import com.grabski.mike.featuretracking.detector.Keypoint
import com.grabski.mike.featuretracking.detector.ORBOpenCVTracker
import com.grabski.mike.featuretracking.detector.ObjectTracker
import com.grabski.mike.featuretracking.utils.C
import com.grabski.mike.featuretracking.view.CameraSurfaceView
import com.grabski.mike.featuretracking.view.OverlayGLView
import com.grabski.mike.featuretracking.view.OverlayView


/**
 * This is the entry point of the application.
 * Some of the code is written in Kotlin instead of Java.
 *
 * This class takes care of initializing needed detectors and views.
 */
open class MainActivity : AppCompatActivity() {

    //Views
    private lateinit var cameraRenderer: CameraSurfaceView
    private lateinit var overlayView: OverlayView
    private lateinit var overlayGLView: OverlayGLView
    private lateinit var msCountText: TextView
    private lateinit var matchCountText: TextView

    //Detector
    private lateinit var detector: ObjectTracker


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //View initialization
        setContentView(R.layout.activity_main)
        cameraRenderer = findViewById(R.id.camera_renderer)
        overlayView = findViewById(R.id.overlay)
        overlayGLView = findViewById(R.id.overlayGLView)
        msCountText = findViewById(R.id.time_text)
        matchCountText = findViewById(R.id.match_count)

        //Detector initialization
        detector = ORBOpenCVTracker(cameraRenderer.frameContainer)
        detector.setTimeMeterListener { elapsedTime ->
            runOnUiThread { msCountText.text = getString(R.string.perframe_time_ms, elapsedTime) }
        }
        detector.addListener(object : FeatureDetectorListener<Keypoint> {
            override fun onKeypointsDetected(keyPoints: MutableList<Keypoint>?) {

            }

            override fun onKeypointsMatched(matchCount: Int, matchedKeypoints: MutableList<Pair<Keypoint, Keypoint>>?) {
                runOnUiThread { matchCountText.text = getString(R.string.matched_number, matchCount) }
            }

        })
    }

    /**
     * Called when start/stop is clicked.
     * Prepares the views and launches the detector.
     */
    fun toggleTracking(view: View) {
        if (view is Button) {
            if (!detector.isTracking) {
                view.text = getString(R.string.stop)
                detector.startTracking()
                overlayView.initWithDetector(detector)
                overlayGLView.setDetector(detector)
            } else {
                view.text = getString(R.string.start)
                detector.stopTracking()
                overlayView.reset()
            }
        }
    }

    ////////////////// Activity Lifecicle Methods \\\\\\\\\\\\\\\\\\\\

    public override fun onPause() {
        super.onPause()
        cameraRenderer.pause()
    }

    public override fun onResume() {
        super.onResume()
        if (ContextCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
            cameraRenderer.initCamera()
        } else {
            ActivityCompat.requestPermissions(this, arrayOf(android.Manifest.permission.CAMERA), C.PERMISSION_CAMERA)
        }
    }

    override fun onDestroy() {
        cameraRenderer.pause()
        App.context = null
        super.onDestroy()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        if (requestCode == C.PERMISSION_CAMERA && grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//            mainContainer.addView(cameraRenderer)
            cameraRenderer.initCamera()
        } else super.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

}
