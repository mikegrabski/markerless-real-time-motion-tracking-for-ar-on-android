package com.grabski.mike.featuretracking.utils;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;

/**
 * This class handles collection of data from the GRV sensor and its "saving"
 */
public class GRVCoordinates implements SensorEventListener {

    //the rotation matrix provided from the sensor
    private float[] rotationMatrix;

    public GRVCoordinates(Context context) {

        SensorManager mSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);

        Sensor gameRotationVector = mSensorManager.getDefaultSensor(Sensor.TYPE_GAME_ROTATION_VECTOR);

        mSensorManager.registerListener(this, gameRotationVector, SensorManager.SENSOR_DELAY_GAME);

    }

    /**
     * Gives you the most recent rotation matrix
     *
     * @return returns a rotation matrix of size 16 (4x4)
     */
    public float[] getRotationMatrix() {
        return rotationMatrix;
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if (sensorEvent.sensor.getType() == Sensor.TYPE_GAME_ROTATION_VECTOR) {
            float[] values = sensorEvent.values.clone();
            rotationMatrix = new float[16];
            SensorManager.getRotationMatrixFromVector(rotationMatrix, values);
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int i) {
    }
}